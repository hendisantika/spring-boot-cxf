package com.hendisantika.springbootcxf.service;

import com.hendisantika.springbootcxf.model.Greeting;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cxf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/03/18
 * Time: 17.09
 * To change this template use File | Settings | File Templates.
 */
@Service
public class GreetingServiceImpl implements GreetingService {

    @Override
    public Greeting sayHello(String name) {
        Greeting greeting = new Greeting();
        greeting.setMessage("Hello " + name + "!!!");
        greeting.setDate(new Date());
        return greeting;
    }

    @Override
    public Greeting sayBye(String name) {
        Greeting greeting = new Greeting();
        greeting.setMessage("Bye " + name + "!!!");
        greeting.setDate(new Date());
        return greeting;
    }
}
package com.hendisantika.springbootcxf.service;

import com.hendisantika.springbootcxf.model.Greeting;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cxf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/03/18
 * Time: 21.28
 * To change this template use File | Settings | File Templates.
 */
@Service
public class InfoServiceImpl implements InfoService {

    @Override
    public Greeting sayHowAreYou(String name) {
        Greeting greeting = new Greeting();
        greeting.setMessage("How are you " + name + "!!!");
        greeting.setDate(new Date());
        return greeting;
    }
}
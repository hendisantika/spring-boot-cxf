package com.hendisantika.springbootcxf.service;

import com.hendisantika.springbootcxf.model.Greeting;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cxf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/03/18
 * Time: 05.56
 * To change this template use File | Settings | File Templates.
 */
@WebService(serviceName = "GreetingService")
public interface GreetingService {

    @WebMethod()
    @WebResult(name = "Greeting")
    Greeting sayHello(@WebParam(name = "GreetingsRequest") String name);

    @WebMethod()
    @WebResult(name = "Greeting")
    Greeting sayBye(@WebParam(name = "GreetingsRequest") String name);
}
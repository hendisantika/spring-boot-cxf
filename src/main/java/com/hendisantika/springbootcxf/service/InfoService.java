package com.hendisantika.springbootcxf.service;

import com.hendisantika.springbootcxf.model.Greeting;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cxf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/03/18
 * Time: 21.26
 * To change this template use File | Settings | File Templates.
 */
@WebService(serviceName = "InfoService")
public interface InfoService {
    @WebMethod()
    @WebResult(name = "Greeting")
    Greeting sayHowAreYou(@WebParam(name = "GreetingsRequest") String name);
}
package com.hendisantika.springbootcxf.service.interceptor;

import org.apache.cxf.ext.logging.LoggingOutInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.OutInterceptors;
import org.apache.cxf.message.Message;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-cxf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/03/18
 * Time: 17.12
 * To change this template use File | Settings | File Templates.
 */
@OutInterceptors
public class AppOutboundInterceptor extends LoggingOutInterceptor {

    @Override
    public void handleMessage(Message message) throws Fault {
        processPayLoad(message);
        super.handleMessage(message);
    }

    private void processPayLoad(Message message) {
        System.out.println("*** PROCESSING PAYLOAD AT OUT-INTERCEPTOR **");
    }
}
